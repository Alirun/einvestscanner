const Web3 = require('web3')
const Promise = require('bluebird')

const { config } = require('../config')
const { etherscan } = require('../utils/etherscan')
const { meta } = require('../db/helpers/meta')
const { stats } = require('../db/helpers/stats')
const { logger } = require('../utils/logger')

const log = logger('loop')

let isLooping = false
let intervalId

const loop = async () => {
  if (isLooping) {
    return
  }

  isLooping = true

  // TODO: Add DB transactions to revert changes in case of error

  // Loop initialization
  log.info('Loop started', new Date())

  const lastBlock = await etherscan.getLastBlockNumber()
  const dbStrategy = await meta.getStrategy()
  const strategy = require(`../utils/strategies/${dbStrategy.name}`)(dbStrategy.params)
  await stats.updateAllowed(lastBlock, strategy)

  isLooping = false
}

const start = async () => {
  intervalId = setInterval(() => {
    loop()  
      .catch(e => {
        log.error(e)
        isLooping = false
      })
  }, config.apps.loop.interval)
}

const stop = () => {
  return new Promise((resolve, reject) => {
    clearInterval(intervalId)
    // wait until loop is finished
    while (isLooping) {}
    resolve()
  })
}

module.exports.loop = {
  start,
  stop
}

const Web3 = require('web3')
const Promise = require('bluebird')

const { config } = require('../config')
const { etherscan } = require('../utils/etherscan')
const { meta } = require('../db/helpers/meta')
const { normalTransaction } = require('../db/helpers/normalTransaction')
const { internalTransaction } = require('../db/helpers/internalTransaction')
const { chart } = require('../db/helpers/chart')
const { logger } = require('../utils/logger')

const log = logger('sync')

let isSyncing = false
let intervalId

const sync = async () => {
  if (isSyncing) {
    return
  }

  isSyncing = true

  // TODO: Add DB transactions to revert changes in case of error

  // Sync initialization
  log.info('Synchronization started', new Date())
  const latestSyncedBlock = await meta.getLatestSyncedBlock()
  const lastBlock = await etherscan.getLastBlockNumber()

  // Normal transactions
  const txs = await etherscan.getNormalTransactions(config.contract.address, latestSyncedBlock, lastBlock)
  for (const tx of txs) {
    await normalTransaction.importTransaction(tx)
  }
  log.info(`Synced ${txs.length} normal transactions`)

  // Set first block
  if (txs.length && latestSyncedBlock == 0) {
    await meta.setFirstBlock(txs[0].blockNumber)
  }
  
  // Internal transactions
  const itxs = await etherscan.getInternalTransactions(config.contract.address, latestSyncedBlock, lastBlock)
  for (const itx of itxs) {
    await internalTransaction.importTransaction(itx)
  }
  log.info(`Synced ${itxs.length} internal transactions`)
  
  // Charts
  await chart.updateChartWithTransactions(txs, itxs)
  log.info(`Chart synced`)

  // Post-sync actions
  await meta.setLatestSyncedBlock(lastBlock)

  await meta.setLatestSyncTimestamp(Math.ceil(Date.now() / 1000))
  
  // TODO: check addressBalanceAfter sync for sync validation purposes
  const addressBalance = await etherscan.getAddressBalance(config.contract.address)
  await meta.setAddressBalance(addressBalance)

  isSyncing = false
}

const start = async () => {
  intervalId = setInterval(() => {
    sync()  
      .catch(e => {
        log.error(e)
        isSyncing = false
      })
  }, config.apps.sync.interval)
}

const stop = () => {
  return new Promise((resolve, reject) => {
    clearInterval(intervalId)
    // wait until syncing is finished
    while (isSyncing) {}
    resolve()
  })
}

module.exports.sync = {
  start,
  stop,
  sync
}

// const web3 = new Web3(new Web3.providers.HttpProvider(config.infura.url))

// Get balance
// web3.eth.getBalance(config.contract.address)
//   .then(balance => {
//     log.info('Web3 balance: ', web3.utils.fromWei(balance, 'ether'))
//   })
//   .catch(e => {
//     log.error(e)
//   })

// etherscan.getAddressBalance(config.contract.address)
//   .then(balance => {
//     log.info('Etherscan balance: ', web3.utils.fromWei(web3.utils.toBN(balance), 'ether'))
//   })
//   .catch(e => {
//     log.error(e)
//   })

const express = require('express')

const { APIError } = require('../../utils/APIError')

const { chart } = require('../../../../db/helpers/chart')

const router = express.Router({ mergeParams: true })

router.get('/chart', async function GetChart(req, res, next) {
  const result = await chart.getChart()
  res.send(result)
})

module.exports.router = router
const express = require('express')

const { APIError } = require('../../utils/APIError')

const { meta } = require('../../../../db/helpers/meta')

const router = express.Router({ mergeParams: true })

router.get('/meta', async function GetMeta(req, res, next) {
  const result = await meta.getMeta()
  res.send(result)
})

module.exports.router = router
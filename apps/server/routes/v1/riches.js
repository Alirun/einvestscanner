const express = require('express')
const Web3 = require('web3')
const { APIError } = require('../../utils/APIError')

const { stats } = require('../../../../db/helpers/stats')

const router = express.Router({ mergeParams: true })
const web3 = new Web3()
const fromWei = (bn) => web3.utils.fromWei(bn.toString(), 'ether')

router.get('/riches', async function GetRiches(req, res, next) {
  const result = await stats.getTopInvested()
  try {
    // result.forEach(element => {
    //   console.log(element.invested)
    //   element.invested = fromWei(element.invested)
    //   element.allowed = web3.utils.fromWei(web3.utils.BN(element.allowed), 'ether')
    //   element.withdrawed = web3.utils.fromWei(web3.utils.BN(element.withdrawed), 'ether')
    // })
    res.send(result)
  } catch (e) {
    console.error(e)
    res.send('err' + e)
  }
})

module.exports.router = router
const express = require('express')
const router = express.Router({ mergeParams: true })
const fs = require('fs')

fs.readdirSync(__dirname).filter(function (file) {
  return (file.indexOf('.') !== 0) && (file !== 'index.js')
}).forEach(function (route) {
  router.use(require(`${__dirname}/${route}`).router)
})

module.exports.router = router

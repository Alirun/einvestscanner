const fs = require('fs')

const routes = {}

fs.readdirSync(__dirname).filter(function (file) {
  return (file.indexOf('.') !== 0) && (file !== 'index.js')
}).forEach(function (version) {
  routes[version] = require(`${__dirname}/${version}`).router
})

module.exports.routes = routes

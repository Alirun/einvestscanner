const express = require('express')
const HttpShutdown = require('http-shutdown')
const bunyanRequest = require('bunyan-request')
const bodyParser = require('body-parser')
const expressWs = require('express-ws')
const cors = require('cors')
const Promise = require('bluebird')

// MUST BE HERE
const app = express()
const ws = expressWs(app)

const { logger } = require('../../utils/logger')
const { config } = require('../../config');
const { routes } = require('./routes')
const { APIError } = require('./utils/APIError')

const log = logger('api-server')
const requestLogger = logger('Request')

app.set('x-powered-by', false)

app.use(bodyParser.json())
app.use(cors(config.apps.server.corsOptions))

app.use(bunyanRequest({
  logger: requestLogger,
  headerName: 'x-request-id'
}))

Object.keys(routes).forEach(route => {
  app.use(`/${route}`, routes[route])
})

app.use(APIError.middleware) // Use APIError middleware from APIError model

app.get('/healthy', (req, res, next) => res.sendStatus(200))

let server

const start = () => {
  return new Promise((resolve, reject) => {
    server = app.listen(config.apps.server.port, () => {
      log.info('Express server listening on port ' + config.apps.server.port)
      resolve()
    })
    
    server = server && HttpShutdown(server) // Wrap server with http-shutdown function to correctly close keep-alive connections
  })
}

const stop = () => {
  return new Promise((resolve, reject) => {
    log.info('Stopping Express server')

    log.info('Telling Websockets to close')
    ws.getWss().clients.forEach(socket => {
      socket.terminate()
    })

    return server && server.shutdown(() => {
      log.info('Express server stopped')
      resolve()
    })
  })
}

module.exports.server = {
  start,
  stop
}
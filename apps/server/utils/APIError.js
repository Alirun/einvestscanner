const VError = require('verror')
const { config } = require('../../../config') 
const { logger } = require('../../../utils/logger')

const log = logger('APIError')

const CODES = {
  NotAcceptable: 'NotAcceptable',
  NotFound: 'NotFound',
  InternalError: 'InternalError',
  ValidationError: 'ValidationError',
  IncorrectPassword: 'IncorrectPassword',
  Unauthorized: 'Unauthorized',
  NoContent: 'NoContent',
  Forbidden: 'Forbidden',
  UnprocessableEntity : 'UnprocessableEntity'
}

const APIErrorResponses = {
  NotAcceptable: 406,
  NotImplemented: 510,      
  BadRequest: 400,
  Unauthorized: 401,
  Forbidden: 403,
  NotFound: 404, 
  Conflict: 409, 
  UnprocessableEntity: 422,
  InternalError: 500,
  NoContent: 204
}
 
class APIError extends VError {
  constructor(errorCode, responseCode, debug, msg) {
    super();
    this.code = errorCode;
    this.responseCode = responseCode;
    this.msg = msg
    if (config.environment === 'development') {
        this.debug = debug;
    }
  }
 
  static middleware(errorsInstance, req, res, next) {
    // APIError errorInstance 
    let error 
    if (errorsInstance){
      error = errorsInstance
      delete error.jse_shortmsg
      delete error.jse_info
    } else {
      error = new APIError(APIError.CODES.InternalError, ResponseCodes.InternalError)
    }
    res.status(error.responseCode || 500).send(error)
  }
}

APIError.APIErrorResponses = APIErrorResponses
APIError.CODES = CODES

module.exports.APIError = APIError
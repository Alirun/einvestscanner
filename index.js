const { logger } = require('./utils/logger')
const { sync } = require('./apps/sync')
const { loop } = require('./apps/loop')
const { server } = require('./apps/server')

const { MongoDriver } = require('./db/storage/MongoDriver')

const log = logger('main')

const main = async () => {
  log.info('Starting EasyInvestScanner')
  await MongoDriver.openConnection()
  await server.start()
  // await sync.start()
  await sync.sync()
  await loop.start()
}

main()
  .catch(e => {
    log.error(e)
  })


process.on('SIGINT', async () => {
  log.warn('Stopping application')
  await loop.stop()
  // await sync.stop()
  await server.stop()
  await MongoDriver.closeConnection()
  process.exit(0)
})

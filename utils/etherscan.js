const VError = require('verror')

const { getJSON } = require('./request')
const { config: { etherscan: { url, apiKey } } } = require('../config')
const { logger } = require('./logger')

const log = logger('etherscan-api')

const etherscanApiCall = async ({ qs }) => {
  const res = await getJSON({ uri: url, qs: {
    ...qs,
    apiKey
  }})

  if (res.message === 'NOTOK') {
    log.error('Error while fetching', { res })
    throw new VError('Etherscan responded with NOTOK')
  }

  return res
}

const getAddressBalance = async (address) => {
  const res = await etherscanApiCall({
    qs: {
      module: 'account',
      action: 'balance',
      address,
      tag: 'latest'
    }
  })

  log.info(`Got address balance ${res.result}`)

  return res.result
}

const getNormalTransactions = async (address, startblock = 0, endblock = 'latest', sort = 'asc') => {
  const contractAddress = address.toLocaleLowerCase()
  const res = await etherscanApiCall({
    qs: {
      module: 'account',
      action: 'txlist',
      address,
      startblock: startblock && startblock + 1 || 0, // If not from beginning, get from latest + 1
      endblock,
      sort
    }
  })

  log.info(`Got ${res.result.length} normal transactions`)

  return res.result.filter(tx => !tx.contractAddress && tx.isError === '0' && tx.to === contractAddress)
}

const getInternalTransactions = async (address, startblock = 0, endblock = 'latest', sort = 'asc') => {
  const contractAddress = address.toLocaleLowerCase()
  const res = await etherscanApiCall({
    qs: {
      module: 'account',
      action: 'txlistinternal',
      address,
      startblock: startblock && startblock + 1 || 0, // If not from beginning, get from latest + 1
      endblock,
      sort
    }
  })

  log.info(`Got ${res.result.length} internal transactions`)
  // TODO: Handle Internal transactions from other contracts
  return res.result.filter(tx => !tx.contractAddress && tx.isError === '0' && tx.from === contractAddress)
}

const getLastBlockNumber = async () => {
  const res = await etherscanApiCall({
    qs: {
      module: 'proxy',
      action: 'eth_blockNumber'
    }
  })

  const blockNumber = parseInt(res.result, 16)

  log.info(`Got ${res.result} -> ${blockNumber} as last block number`)
  return blockNumber
}

const etherscan = {
  getAddressBalance,
  getNormalTransactions,
  getInternalTransactions,
  getLastBlockNumber
}

module.exports.etherscan = etherscan

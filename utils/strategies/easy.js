const dailyBlocks = 5900

const easy = ({ percent }) => ({ lastBlock, currentBlock, invested }) => {
  const diff = currentBlock - lastBlock
  return invested.multipliedBy( diff * (percent / 100) / dailyBlocks)
}

module.exports = easy

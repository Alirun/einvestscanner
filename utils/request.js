const VError = require('verror')
const request = require('request-promise')

const { logger } = require('./logger')

const log = logger('request')

const getJSON = async ({ uri, qs }) => {
  log.debug('Request', { uri, qs })
  try {
    return await request({
      uri,
      qs,
      json: true
    })
  } catch (e) {
    throw new VError('RequestError').cause(e)
  }
}

module.exports.getJSON = getJSON

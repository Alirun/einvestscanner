const bunyan = require('bunyan')
const bformat = require('bunyan-format')

const { config } = require('../config/')

const formatOut = bformat({ outputMode: 'short' })

module.exports.logger = function createLogger(name) {
  const loggerOptions = {
    name: name || 'default',
    streams: [
      {
        stream: formatOut,
        level: config.logger.level
      }
    ],
    serializers: {
      err: bunyan.stdSerializers.err
    }
  }

  return bunyan.createLogger(loggerOptions)
}
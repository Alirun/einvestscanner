const { MongoDriver } = require('../storage/MongoDriver')
const { logger } = require('../../utils/logger')
const { Meta } = require('../storage/models')

const log = logger('seed')

const easyInvest4 = new Meta({
  pyramid: 'easyInvest4',
  strategy: {
    name: 'easy',
    params: {
      percent: 4
    }
  }
})

MongoDriver.openConnection()
  .then(MongoDriver.dropDB.bind(MongoDriver))
  .then(async () => {
    await MongoDriver.save(easyInvest4)
  })
  .then(() => log.info("Success!"))
  .then(MongoDriver.closeConnection.bind(MongoDriver))
  .catch((e) => log.error(e))

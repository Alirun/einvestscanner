const VError = require('verror')
const _ = require('lodash')
const d3 = require('d3')

const { config } = require('../../config')
const { MongoDriver } = require('../storage/MongoDriver')
const { Chart } = require('../storage/models')
const { logger } = require('../../utils/logger')
const { meta } = require('./meta')

const log = logger('chart')

let firstBlock = 0

const _findLatestChartChunk = async() => {
  const { records: chunks } = await MongoDriver.find({ model: Chart, limit: 1, sort: '-_id' })

  if (!chunks.length) {
    return new Chart()
  }

  return chunks[0]
}

const _findChartChunk = async(hash) => {
  const { records: chunks } = await MongoDriver.find({ model: Chart, data: { hash } })

  if (!chunks.length) {
    log.error(new VError('No chart chunk was found for hash', hash))
    return await _findLatestChartChunk()
  }

  return chunks[0]
}

const getChart = async () => {
  const { records: chart } = await MongoDriver.find({ model: Chart })
  
  return chart
}

const _updateChartWithNormalTransaction = async (_tx, _strategy) => {
  const latestChunk = await _findLatestChartChunk()

  const chunk = new Chart({
    hash: _tx.hash,
    timeStamp: _tx.timeStamp,
    blockNumber: _tx.blockNumber,
    balance: latestChunk.balance.plus(_tx.value),
    invested: latestChunk.invested.plus(_tx.value),
    withdrawed: latestChunk.withdrawed,
    allowed: _strategy({
      invested: latestChunk.invested,
      lastBlock: firstBlock,
      currentBlock: _tx.blockNumber
    })
  })

  await MongoDriver.save(chunk)
}

const _updateChartWithInternalTransaction = async (_itx, _strategy) => {
  const chunk = await _findChartChunk(_itx.hash)
  chunk.balance = chunk.balance.minus(_itx.value)
  chunk.withdrawed = chunk.withdrawed.plus(_itx.value)

  await MongoDriver.save(chunk)
}

const updateChartWithTransactions = async (_txs, _itxs) => {
  // Import strategy for current pyramid
  const dbStrategy = await meta.getStrategy()
  const strategy = require(`../../utils/strategies/${dbStrategy.name}`)(dbStrategy.params)

  // Import first block
  firstBlock = await meta.getFirstBlock()

  // Get contract address TODO: Move to Meta
  const contractAddress = config.contract.address.toLocaleLowerCase()

  // Prepare transactions and updateChart
  const txs = _.concat(_txs, _itxs) // concat normal and internal transactions
  const grouped = _.groupBy(txs, 'hash') // group by hash
  for (const hash in grouped) {
    for (const tx of grouped[hash]) {
      if (tx.from !== contractAddress) { // normal
        await _updateChartWithNormalTransaction(tx, strategy)
      } else { // internal
        await _updateChartWithInternalTransaction(tx, strategy)
      }
    }
  }
}

const chart = {
  getChart,
  updateChartWithTransactions,
}

module.exports.chart = chart

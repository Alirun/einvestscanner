const { MongoDriver } = require('../storage/MongoDriver')
const { InternalTransaction } = require('../storage/models')
const { stats } = require('./stats')
const { meta } = require('./meta')

const importTransaction = async (_tx) => {
  const tx = new InternalTransaction({
    blockNumber: _tx.blockNumber,
    timeStamp: _tx.timeStamp,
    to: _tx.to,
    hash: _tx.hash,
    value: _tx.value
  })
  await MongoDriver.save(tx)

  const dbStrategy = await meta.getStrategy()
  const strategy = require(`../../utils/strategies/${dbStrategy.name}`)(dbStrategy.params)
  
  await stats.updateWithInternalTransaction(tx, strategy)
  await meta.incrementTotalWithdrawed(tx.value)
  
  return tx
}

const internalTransaction = {
  importTransaction
}

module.exports.internalTransaction = internalTransaction

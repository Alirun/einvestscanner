const { MongoDriver } = require('../storage/MongoDriver')
const { NormalTransaction } = require('../storage/models')
const { stats } = require('./stats')
const { meta } = require('./meta')

const importTransaction = async (_tx) => {
  const tx = new NormalTransaction({
    blockNumber: _tx.blockNumber,
    timeStamp: _tx.timeStamp,
    from: _tx.from,
    hash: _tx.hash,
    value: _tx.value
  })
  await MongoDriver.save(tx)

  const dbStrategy = await meta.getStrategy()
  const strategy = require(`../../utils/strategies/${dbStrategy.name}`)(dbStrategy.params)

  await stats.updateWithNormalTransaction(tx, strategy)
  await meta.incrementTotalInvested(tx.value)
  
  return tx
}

const normalTransaction = {
  importTransaction
}

module.exports.normalTransaction = normalTransaction

const { MongoDriver } = require('../storage/MongoDriver')
const { Stats } = require('../storage/models')

const _getStats = async (address) => {
  const { records: stats } = await MongoDriver.find({model: Stats, data: { address }, limit: 1})

  if (!stats.length) {
    const stat = new Stats({ address })
    await MongoDriver.save(stat)
    return stat
  }

  return stats[0]
}

const updateWithNormalTransaction = async (_tx, _strategy) => {
  const stat = await _getStats(_tx.from)
  
  // Increment investments
  stat.invested = stat.invested.plus(_tx.value)

  // Set last withdrawed block
  stat.lastWithdrawed = _tx.blockNumber

  await MongoDriver.save(stat)
}

const updateWithInternalTransaction = async (_itx, _strategy) => {
  const stat = await _getStats(_itx.to)
  
  // Increment withdrawed
  stat.withdrawed = stat.withdrawed.plus(_itx.value)

  // Set last withdrawed block
  stat.lastWithdrawed = _itx.blockNumber

  await MongoDriver.save(stat)
}

const getTopInvested = async () => {
  const { records: stats } = await MongoDriver.find({ model: Stats })

  stats.sort((b, a) => a.invested.comparedTo(b.invested)) // Sort from rich to poor

  return stats
}

const updateAllowed = async (_lastBlock, _strategy) => {
  const { records: stats } = await MongoDriver.find({ model: Stats })
  for (const stat of stats) {
    stat.allowed = _strategy({
      invested: stat.invested,
      lastBlock: stat.lastWithdrawed,
      currentBlock: _lastBlock
    })
    await MongoDriver.save(stat)
  }
}

const stats = {
  updateWithNormalTransaction,
  updateWithInternalTransaction,
  getTopInvested,
  updateAllowed
}

module.exports.stats = stats

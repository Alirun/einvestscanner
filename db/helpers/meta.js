const { MongoDriver } = require('../storage/MongoDriver')
const { Meta } = require('../storage/models')

const getMeta = async () => {
  const { records: metas } = await MongoDriver.find({model: Meta, limit: 1})

  if (!metas.length) {
    const meta = new Meta()
    await MongoDriver.save(meta)
    return meta
  }

  return metas[0]
}

const getLatestSyncedBlock = async () => {
  const meta = await getMeta()

  return meta.latestSyncedBlock
}

const setLatestSyncedBlock = async (_latestSyncedBlock) => {
  const meta = await getMeta()
  meta.latestSyncedBlock = _latestSyncedBlock
  await MongoDriver.save(meta)
}

const getFirstBlock = async () => {
  const meta = await getMeta()

  return meta.firstBlock
}

const setFirstBlock = async (_firstBlock) => {
  const meta = await getMeta()
  meta.firstBlock = _firstBlock
  await MongoDriver.save(meta)
}

const setLatestSyncTimestamp = async (_latestSyncTimestamp) => {
  const meta = await getMeta()
  meta.latestSyncTimestamp = _latestSyncTimestamp
  await MongoDriver.save(meta)
}

const incrementTotalInvested = async (_value) => {
  const meta = await getMeta()
  meta.totalInvested = meta.totalInvested.plus(_value)
  await MongoDriver.save(meta)
}

const incrementTotalWithdrawed = async (_value) => {
  const meta = await getMeta()
  meta.totalWithdrawed = meta.totalWithdrawed.plus(_value)
  await MongoDriver.save(meta)
}

const setAddressBalance = async (_addressBalance) => {
  const meta = await getMeta()
  meta.addressBalance = _addressBalance
  await MongoDriver.save(meta)
}

const getStrategy = async () => {
  const meta = await getMeta()
  
  return meta.strategy
}

const meta = {
  setLatestSyncedBlock,
  getLatestSyncedBlock,

  setFirstBlock,
  getFirstBlock,

  setLatestSyncTimestamp,

  incrementTotalInvested,
  incrementTotalWithdrawed,

  setAddressBalance,

  getMeta,

  getStrategy
}

module.exports.meta = meta

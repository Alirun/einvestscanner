const mongoose = require('mongoose')
const BigNumber = require('bignumber.js')
const BigNumberSchema = require('mongoose-bignumber')
const ObjectId = mongoose.Schema.Types.ObjectId

const NormalTransactionSchema = {
  blockNumber: Number,
  timeStamp: Number,
  from: {
    type: String,
    index: true
  },
  hash: {
    type: String,
    unique: true
  },
  value: BigNumberSchema
}

const InternalTransactionSchema = {
  blockNumber: Number,
  timeStamp: Number,
  to: {
    type: String,
    index: true
  },
  hash: {
    type: String,
    unique: true
  },
  value: BigNumberSchema
}

const StatsSchema = {
  address: {
    type: String,
    unique: true,
    index: true
  },
  invested: {
    type: BigNumberSchema,
    default: new BigNumber('0')
  },
  withdrawed: {
    type: BigNumberSchema,
    default: new BigNumber('0')
  },
  allowed: {
    type: BigNumberSchema,
    default: new BigNumber('0')
  },
  lastWithdrawed: {
    type: Number,
    default: 0
  }
}

const MetaSchema = {
  pyramid: {
    type: String,
    unique: true,
    indexed: true
  },
  latestSyncedBlock: {
    type: Number,
    default: 0
  },
  latestSyncTimestamp: {
    type: Number,
    default: 0
  },
  totalInvested: {
    type: BigNumberSchema,
    default: new BigNumber('0')
  },
  totalWithdrawed: {
    type: BigNumberSchema,
    default: new BigNumber('0')
  },
  addressBalance: {
    type: BigNumberSchema,
    default: new BigNumber('0')
  },
  firstBlock: {
    type: Number,
    default: 0
  },
  strategy: {
    name: String,
    params: {
      percent: Number
    }
  }
}

const ChartSchema = {
  hash: {
    type: String,
    unique: true,
    index: true
  },
  blockNumber: Number,
  timeStamp: Number,
  balance: {
    type: BigNumberSchema,
    default: new BigNumber('0')
  },
  invested: {
    type: BigNumberSchema,
    default: new BigNumber('0')
  },
  withdrawed: {
    type: BigNumberSchema,
    default: new BigNumber('0')
  },
  allowed: {
    type: BigNumberSchema,
    default: new BigNumber('0')
  }
}

module.exports.NormalTransaction = mongoose.model('NormalTransaction', NormalTransactionSchema)
module.exports.InternalTransaction = mongoose.model('InternalTransaction', InternalTransactionSchema)
module.exports.Stats = mongoose.model('Stats', StatsSchema)
module.exports.Meta = mongoose.model('Meta', MetaSchema)
module.exports.Chart = mongoose.model('Chart', ChartSchema)
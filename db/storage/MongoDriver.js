const mongoose = require('mongoose')
const Promise = require('bluebird')
const VError = require('verror')

const { logger } = require('../../utils/logger')
const { config: { database: { name, host } } } = require('../../config')

const log = logger('MongoDriver')

class MongoDriver {
  async openConnection() {
    await mongoose.connect(`mongodb://${host}/${name}`)
    this.db = mongoose.connection
  }

  save(modelInstance) {
    return new Promise((resolve, reject) => {
      modelInstance.save((err) => {
        if (err) return reject(err)
        resolve()
      })
    })
  }

  edit({ model, id, data }) {
    return new Promise((resolve, reject) => {
      model.findOneAndUpdate({ _id: id }, data, { new: true }, ((err, records) => {
        if (err) { return reject(err) }
        resolve({ records })
      }))
    })
  }

  delete({ model, id }) {
    return new Promise((resolve, reject) => {
      model.findOneAndRemove({ _id: id }, (err, doc) => {
        if (err) reject(err)
        resolve({ doc })
      })
    })
  }

  count({ model, data }) {
    return new Promise((resolve, reject) => {
      model.count(data, (err, count) => {
        if (err) return reject(err)
        resolve({ count })
      })
    })
  }

  find({ model, data, limit = 0, sort, options, select, populateParam, collation }) {
    return new Promise((resolve, reject) => {
      if (populateParam) {
        model.find(data, select, options).sort(sort).limit(limit).populate(populateParam).collation(collation).exec((err, records) => {
          if (err) return reject(err)
          resolve({ records })
        })
      } else {
        model.find(data, select, options).sort(sort).limit(limit).collation(collation).exec((err, records) => {
          if (err) return reject(err)
          resolve({ records })
        })
      }
    })
  }

  closeConnection() {
    return new Promise((resolve, reject) => {
      this.db.close(true, () => resolve())
    })
  }

  async dropDB() {
    if (this.db) {
      return this.db.dropDatabase()
    }

    throw new VError('DB Instance is not present')
  }

}



module.exports.MongoDriver = new MongoDriver()
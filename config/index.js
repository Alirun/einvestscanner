const environment = process.env.NODE_ENV || 'development'
const port = process.env.PORT || 3000

const MongoDBName = process.env.MONGODB_NAME || 'eis'
const MongoDBHost = process.env.MONGODB_HOST || 'localhost:27017'

const logLevels = {
  development: 'debug',
  production: 'info'
}

const config = {
  environment,
  
  logger: {
    level: logLevels[environment] || 'debug'
  },

  database: {
    name: MongoDBName,
    host: MongoDBHost
  },

  infura: {
    url: 'https://mainnet.infura.io/v3/0787bcba4a7a4e8db3b1f59e68fc35ce'
  },

  etherscan: {
    url: 'https://api.etherscan.io/api',
    apiKey: '7GWKWIZJBUYMXH8J777S3PMIGS7P4QHUAG'
  },

  contract: {
    address: '0x7B307C1F0039f5D38770E15f8043b3dD26da5E8f'
  },

  apps: {
    server: {
      corsOptions: {
        exposedHeaders: ['x-pages-count']
      },
      port
    },

    sync: {
      interval: 1000 * 30 // Every 30 seconds
    },

    loop: {
      interval: 1000 * 13 // Every 13 seconds
    }
  }
}

module.exports.config = config

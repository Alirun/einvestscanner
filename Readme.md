# EIS - EasyInvestScanner

Uses Infura and etherscan.io APIs to create a statistics analysis on pyramid contracts

## Installation

Install npm dependencies

Run MongoDB

```
  docker-compose up
```

Run Sync + API server (takes some time to synchronize)

```
  node .
```

## API

### Get riches list

```
  GET localhost:3000/v1/riches
```

### Get meta

```
  GET localhost:3000/v1/meta
```

### Get main chart

```
  GET localhost:3000/v1/chart
```

## TODOs

- [ ] Aggredated data for totalAllowed: db, helpers, api
- [ ] Aggregated data and tables for graphs and charts: TBD
- [ ] Add stats: firstInvested, investedByTime chart data, totalWithdrawAvailable calculation
- [ ] BUG: Properly handle Internal transactions from other contracts (https://etherscan.io/tx/0x0b5578899c80741e3dc423b0559b0f1e6a3b02bde02cd18e6f705cdb2c9aaada)
- [ ] LATE: One DB -> One contract
- [ ] LATE: Get address from meta instead of config
- [ ] LATE: Set db's and metas in seed

## Useful links

- https://etherconverter.online/ Ethereum values converted

## PaaS Ideas

Check your investments online, set alerts

- Public charts for free (example https://beta.observablehq.com/@phantomydn/analysis-of-eth-pyramid)
- Pay eth for premium
- - Get riches stats
- - Set alarms: email, telegram, sms
- First 5 test accounts for free forever
- Payment: 1) 1 Pyramid - 1 month -> 0.05 ETH   2) 1 Pyramid - permanent -> 0.1 ETH   3) All pyramids - 1 month -> 0.1 ETH   4) All pyramids - permanent -> 1 ETH
- Payment method: Smart contract
- Website feedback: Want to have another pyramid? Write us!
- Marketing: Telegram, Bitcointalk, Youtube?

## Other pyramids list

http://scmonit.com/

Alive:
- 333 -> 0x311f71389e3DE68f7B2097Ad02c6aD7B2dDE4C71
- Silver -> 0x74731ce1bF0aC7f89F1D4161E1Fd57B11B287e4A
- 5% -> 0x6519d6Dfd11363CF0821809b919B55F794fe0cb5
- Gorgona -> 0x020e13faF0955eFeF0aC9cD4d2C64C513ffCBdec
- 

Died: 
- Pandemica -> 0xD8a1Db7AA1E0ec45E77B0108006dc311cD9D00e7
